// Based on https://voxblox.readthedocs.io/en/latest/pages/Using-Voxblox-for-Planning.html

/**
  Include paths required for Intellisense: 
    "~/catkin_ws/build/voxblox/**",
    "~/catkin_ws/src/voxblox_package/eigen_checks/include/**",
    "~/catkin_ws/src/voxblox_package/minkindr/minkindr/include/**",
    "~/catkin_ws/src/voxblox_package/minkindr_ros/minkindr_conversions/include/**",
    "~/catkin_ws/src/voxblox_package/numpy_eigen/include/**",
    "~/catkin_ws/src/voxblox_package/voxblox/voxblox/include/**",
    "~/catkin_ws/src/voxblox_package/voxblox/voxblox_ros/include/**",
    "~/catkin_ws/src/voxblox_package/voxblox/voxblox_rviz_plugin/include/**" 
*/

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64MultiArray.h>
#include <voxblox_ros/esdf_server.h>
#include <nav_msgs/Odometry.h>



#define RATE 5
#define ROBOT_RADIUS 0.4
#define MIN_DISTANCE 1.0 

/**
 * Voxblox interfacing class to get the distance and 
 * direction to the closest obstacle. 
 */
class PlannerVoxblox {
    public:
    PlannerVoxblox(const ros::NodeHandle& nh,
                        const ros::NodeHandle& nh_private);
    virtual ~PlannerVoxblox() {}
    double getMapDistance(const Eigen::Vector3d& position, 
                            Eigen::Vector3d& gradient, 
                            Eigen::Vector3d& target) const;
    bool generateESDF();
    
    private:
    ros::NodeHandle nh_;
    ros::NodeHandle nh_private_;
    voxblox::EsdfServer voxblox_server_; // Map ! 
};

static PlannerVoxblox* planner_ptr; 
static ros::Publisher map_pub; 

PlannerVoxblox::PlannerVoxblox(const ros::NodeHandle& nh,
                                     const ros::NodeHandle& nh_private)
    : nh_(nh),
      nh_private_(nh_private),
      voxblox_server_(nh_, nh_private_) {
    // Optionally load a map saved with the save_map service call in voxblox.
    std::string input_filepath;
    nh_private_.param("voxblox_path", input_filepath, input_filepath);
    if (!input_filepath.empty()) {
        if (!voxblox_server_.loadMap(input_filepath)) {
            ROS_ERROR("Couldn't load ESDF map!");
            exit(EXIT_FAILURE);
        }
    }
    voxblox_server_.setTraversabilityRadius(ROBOT_RADIUS);
    //voxblox_server_.publishTraversable();
}

double PlannerVoxblox::getMapDistance(
    const Eigen::Vector3d& position, Eigen::Vector3d& gradient, Eigen::Vector3d& target) const {
    
    double distance = 0.0;
    if (!voxblox_server_.getEsdfMapPtr() || 
        !voxblox_server_.getEsdfMapPtr()->getDistanceAndGradientAtPosition(position,
                                                                            &distance,
                                                                            &gradient)){
        //ROS_WARN("Could not get distance from map");
        return 0.0; 
    }else{
        // Perform gradient ascent until distance is sufficient
        const double eta = 0.01; 
        const uint max_steps = 100; 
        uint i = 0; 
        double          _d = distance; 
        Eigen::Vector3d _p = position; 
        Eigen::Vector3d _g = gradient;
         
        while(_d > 0.0 && _d < MIN_DISTANCE && i < max_steps){
            _p += _g*eta; 
            if (!voxblox_server_.getEsdfMapPtr() || 
                !voxblox_server_.getEsdfMapPtr()->
                getDistanceAndGradientAtPosition(_p,&_d,&_g))
                break; 
            i++; 
        }
        target = _p; // Assign the reached pose as target 
    }
    return distance;
}

bool PlannerVoxblox::generateESDF(){
    const voxblox::Point coords(.0f, .1f, .2f); 
    voxblox_server_.getEsdfMapPtr()->getEsdfLayer().getBlockPtrByCoordinates(coords);
    std_srvs::Empty srv; 
    return voxblox_server_.generateEsdfCallback(srv.request, srv.response);
}

/**
 * Get the distance and gradient from the esdf map
 */
void pose_cb(const nav_msgs::Odometry::ConstPtr &msg){

    Eigen::Vector3d position; position.setZero();
    Eigen::Vector3d gradient; gradient.setZero();
    Eigen::Vector3d target;
    target << msg->pose.pose.position.x, 
                msg->pose.pose.position.y, 
                msg->pose.pose.position.z;

    double distance = 0.0;
    position[0] = msg->pose.pose.position.x; 
    position[1] = msg->pose.pose.position.y; 
    position[2] = msg->pose.pose.position.z; 

    //planner_ptr->generateESDF();

    distance = planner_ptr->getMapDistance(position, gradient, target); 

    // DEBUG 
    //std::cout << "Distance:" << distance << std::endl; 
    //std::cout << "Distance gradient:\n" << gradient << std::endl;

    std_msgs::Float64MultiArray map_info; 
    map_info.data.push_back(distance);      // 0 - Distance
    map_info.data.push_back(position[0]);   // 1 - Pose
    map_info.data.push_back(position[1]);
    map_info.data.push_back(gradient[2]);
    map_info.data.push_back(target[0]);     // 7 - Target
    map_info.data.push_back(target[1]);
    map_info.data.push_back(target[2]);
    map_info.data.push_back(gradient[0]);   // 4 - Distance gradient
    map_info.data.push_back(gradient[1]);
    map_info.data.push_back(gradient[2]);


    map_pub.publish(map_info);
}

int main(int argc, char* argv[]){
    ros::init(argc,argv,"voxblox_interface");
    ros::NodeHandle nh; 
    ros::NodeHandle nh_private("~");
    ros::Rate rate(RATE); 

    // Subscribe to the pose topic -> mavros data is not publishing so trying fake rostopic to see if a mesh will be created correctly
    ros::Subscriber pose_sub = nh.subscribe<nav_msgs::Odometry>("/vio/odometry", 1, pose_cb);

    // Advertise gradient and distance
    map_pub = nh.advertise<std_msgs::Float64MultiArray>("/v_map/map_distance_and_gradient", 1);

    // Instantiate interface 
    PlannerVoxblox planner(nh,nh_private);
    planner_ptr = &planner; 
    
    while(ros::ok()){
        ros::spinOnce();
        rate.sleep();
    }
}
